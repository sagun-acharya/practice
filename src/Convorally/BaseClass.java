package Convorally;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.time.Duration;

class BaseClass {
    WebDriver driver;

    BaseClass() {
        this.driver = new ChromeDriver();
    }

    @Test
    public WebDriver getDriver() {
        return driver;
    }
    @Test
    public void setDriver() {
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://qa.convorally.com");
    }
}