package Convorally;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import java.lang.management.ManagementFactory;

@Test
public class LoginPage {
    WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;

    }

    By userName = By.id("username");
    By passWord = By.id("password");
    By loginButton = (By.cssSelector(".btn.d-block.button-gradient"));

    @Test(priority = 0)
    public void enterUsername(String user) {
        driver.findElement(userName).sendKeys(user);
    }

    @Test(priority = 1)
    public void enterPassword(String password) {
        driver.findElement(passWord).sendKeys(password);
    }

    @Test(priority = 2)
    public void clickLogin() {
        driver.findElement(loginButton).click();
    }

}
