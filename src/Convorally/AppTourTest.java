package Convorally;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.time.Duration;

public class AppTourTest {
    WebDriver driver;


    public AppTourTest(WebDriver driver) {
        this.driver = driver;
    }

    By takeATourIcon = By.xpath("//a[@title='Take a tour']");
    By takeATourButton = By.cssSelector(".btn.default-btn.mt-2");

    By nextButton = By.cssSelector(".introjs-button.introjs-nextbutton");

    By doneButton = By.cssSelector(".introjs-button.introjs-nextbutton.introjs-donebutton");
@Test
    public void appTourTest() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        wait.until(ExpectedConditions.and(ExpectedConditions.elementToBeClickable(takeATourIcon), ExpectedConditions.visibilityOfElementLocated(takeATourIcon), ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".loader-wrap"))));

        driver.findElement(takeATourIcon).click();

        driver.findElement(takeATourButton).click();

        for (int i = 0; i < 19; i++) {

            driver.findElement(nextButton).click();
        }

        driver.findElement(doneButton).click();
    }

}
