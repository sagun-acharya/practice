package Convorally;

import org.testng.annotations.Test;

public class NewProjectTC1 {
    @Test
    public static void main(String[] args) {

        // Invoking browser
        BaseClass baseClass = new BaseClass();

        // get driver
        baseClass.setDriver();
        NewProject projectTest = new NewProject(baseClass.getDriver());

        // create project functionality
        projectTest.clickOnCreateProject();
        projectTest.setProjectTitle("My Automated Project!");
        projectTest.setProjectDescription("This is my first automated project");
        projectTest.setAddress("Budhanilkantha");
        projectTest.exitButton();
    }
}
