package Convorally;

import org.testng.annotations.Test;

@Test
public class ConvorallyTest extends BaseClass {

    @Test
    public static void main(String[] args){
        // Invoking browser
        ConvorallyTest convoTest = new ConvorallyTest();

        // get driver
        convoTest.setDriver();

// To: Implement cookie in browser for session saving

//        Cookie cookie = baseClass.getDriver().manage().getCookieNamed("convorally_session");
//        System.out.println(cookie);
//        baseClass.getDriver().manage().addCookie(cookie);
//
//        baseClass.getDriver().navigate().refresh();
//        baseClass.getDriver().get("https://qa.convorally.com/#/dashboard");


        LoginPage login = new LoginPage(convoTest.getDriver());
        AppTourTest appTour = new AppTourTest(convoTest.getDriver());
        NewProject newProjectTest = new NewProject(convoTest.getDriver());

        login.enterUsername("smarikarimal22@gmail.com");
        login.enterPassword("convorally123@");
        login.clickLogin();
        appTour.appTourTest();

        convoTest.getDriver().navigate().refresh();
        newProjectTest.clickOnCreateProject();
        newProjectTest.setProjectTitle("My First Automated Project!");
        newProjectTest.setProjectDescription("This is my first automated project");
        newProjectTest.setAddress("Budhanilkantha");
        newProjectTest.exitButton();


    }

}
