package Convorally;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class NewProject {
    WebDriver driver;

    public NewProject(WebDriver driver) {
        this.driver = driver;
    }

    By createButton = By.cssSelector("#createNewProject");
    By projectTitleTag = By.xpath("(//input[@placeholder=\"Provide the project name\"])[2]");

    By projectDescriptionTag = By.xpath("(//textarea[@placeholder=\"Describe your project\"])[2]");

    By addressTag = By.id("autocomplete");

    By clickButton = By.cssSelector("div[id='createProject'] span[aria-hidden='true']");

    public void clickOnCreateProject() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        wait.until(ExpectedConditions.and(ExpectedConditions.elementToBeClickable(createButton),
                ExpectedConditions.visibilityOfElementLocated(createButton),
                ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".loader-wrap"))));

        driver.findElement(createButton).click();
    }

    public void setProjectTitle(String title) {
        driver.findElement(projectTitleTag).sendKeys(title);

    }

    public void setProjectDescription(String description) {
        driver.findElement(projectDescriptionTag).sendKeys(description);
    }

    public void setAddress(String address) {
        driver.findElement(addressTag).sendKeys(address);
    }

    public void exitButton() {
        driver.findElement(clickButton).click();
    }

}
